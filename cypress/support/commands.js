// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
Cypress.Commands.add('add_step', (x, y) => {
    cy.get('#Block_container')
        .trigger('mousedown')
    cy.get('#diagram_diagramLayer_svg').trigger('mousemove').click(x, y) //.trigger('blur')
})
Cypress.Commands.add('login', (email, password) => {
    // const email = "szymon.szuwalski@aidadx.io"
    // const password = "peHGwSGeMLfK36JWJJfYMkJw"
    cy.visit('https://aidadxtest.b2clogin.com/aidadxtest.onmicrosoft.com/b2c_1_test_aidadx_signin/oauth2/v2.0/authorize?response_type=id_token&scope=openid%20profile%20https%3A%2F%2Faidadxtest.onmicrosoft.com%2Faida-flow-test-web-api%2Fuser_impersonation&client_id=eeddb338-e5fa-4cd4-be0f-a27515d4ce58&redirect_uri=https%3A%2F%2Faidaflowtestpanel.azurewebsites.net%2F&state=eyJpZCI6IjZhZGY2ODE5LTRjODMtNGQ5Yy1iY2FjLTFkMGZmNDlmMWNkZCIsInRzIjoxNjQyNDA0MTMyLCJtZXRob2QiOiJyZWRpcmVjdEludGVyYWN0aW9uIn0%3D&nonce=acedfa26-d9da-4a67-9192-3925d9ddcb0a&client_info=1&x-client-SKU=MSAL.JS&x-client-Ver=1.4.4&client-request-id=29fcec0e-d9a8-433b-907c-504fdae27d5b&response_mode=fragment')
    //Login to the system
    cy.get('input[id="email"]').type(email)
    cy.get('input[id="password"]').type(password)
    cy.get('.buttons').click()
})
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })

import '@4tw/cypress-drag-drop'