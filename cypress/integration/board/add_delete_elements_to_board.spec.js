/// <reference types="cypress" />
describe('Tests aida flow board', () => {
    before(() => {
        //cy.request('GET', 'https://aidadxtest.b2clogin.com/aidadxtest.onmicrosoft.com/b2c_1_test_aidadx_signin/oauth2/v2.0/logout?client-request-id=86116345-524b-4670-bee0-5c5b37c89f58&post_logout_redirect_uri=https%3A%2F%2Faidaflowtestpanel.azurewebsites.net%2F')

    })
    it.only('Drag and drop elements on the board', () => {
        const email = "szymon.szuwalski@aidadx.io"
        const password = "peHGwSGeMLfK36JWJJfYMkJw"
        cy.visit('https://aidadxtest.b2clogin.com/aidadxtest.onmicrosoft.com/b2c_1_test_aidadx_signin/oauth2/v2.0/authorize?response_type=id_token&scope=openid%20profile%20https%3A%2F%2Faidadxtest.onmicrosoft.com%2Faida-flow-test-web-api%2Fuser_impersonation&client_id=eeddb338-e5fa-4cd4-be0f-a27515d4ce58&redirect_uri=https%3A%2F%2Faidaflowtestpanel.azurewebsites.net%2F&state=eyJpZCI6IjZhZGY2ODE5LTRjODMtNGQ5Yy1iY2FjLTFkMGZmNDlmMWNkZCIsInRzIjoxNjQyNDA0MTMyLCJtZXRob2QiOiJyZWRpcmVjdEludGVyYWN0aW9uIn0%3D&nonce=acedfa26-d9da-4a67-9192-3925d9ddcb0a&client_info=1&x-client-SKU=MSAL.JS&x-client-Ver=1.4.4&client-request-id=29fcec0e-d9a8-433b-907c-504fdae27d5b&response_mode=fragment')
        //Login to the system
        cy.get('input[id="email"]').type(email)
        cy.get('input[id="password"]').type(password)
        cy.get('.buttons').click()

        //Press button create flow 
        cy.get('#create-flow-btn').click()

        //add steps to the board
        cy.get('#Block_container')
            .trigger('mousedown')
        cy.get('#diagram_diagramLayer_svg').trigger('mousemove').click(100, 100)
        cy.get('.diagram-block-container-content').first().should('be.visible', 'have.length', 1)
        cy.get('#Block_container')
            .trigger('mousedown')
        cy.get('#diagram_diagramLayer_svg').trigger('mousemove').click(100, 300)
        cy.get('.diagram-block-container-content').eq(1).should('be.visible', 'have.length', 2)
        cy.get('#Block_container')
            .trigger('mousedown')
        cy.get('#diagram_diagramLayer_svg').trigger('mousemove').click(100, 500)
        cy.get('.diagram-block-container-content').eq(2).should('be.visible', 'have.length', 3)
        cy.get('#Block_container')
            .trigger('mousedown')
        cy.get('#diagram_diagramLayer_svg').trigger('mousemove').click()
        cy.get('.diagram-block-container-content').last().should('be.visible', 'have.length', 4)
        //add path flow
        cy.get('#Link_container')
            .trigger('mousedown')
        cy.get('#diagram_diagramLayer_svg').trigger('mousemove').click(50, 150)
        //add end of flow 
        cy.get('#End_container').trigger('mousedown')
        cy.get('#diagram_diagramLayer_svg').trigger('mousemove').click(10, 10)
        cy.get('tspan').should('contain.text', 'Koniec')
    });
    it('Delete elemensts from board', () => {
        //delete step items
        cy.get('.diagram-block-container-content').first().type('{del}')
        cy.get('.diagram-block-container-content').should('have.length', 3)
        cy.get('.diagram-block-container-content').first().type('{del}')
        cy.get('.diagram-block-container-content').should('have.length', 2)
        cy.get('.diagram-block-container-content').first().type('{del}')
        cy.get('.diagram-block-container-content').first().type('{del}')
        cy.get('g > path').eq(1).type('{del}')
        cy.get('g > path').should('have.length', 2)
        cy.get('g > path').eq(0).type('{del}')
        cy.get('#diagram_diagramLayer').should('not.be.visible')



    });

})