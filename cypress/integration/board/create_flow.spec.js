/// <reference types="cypress" />
import faker from 'faker'
import '@4tw/cypress-drag-drop'
describe(('Create flow'), () => {
    it('Add basic flow', () => {
        const title = faker.random.word()
        const words1 = faker.random.words()
        const words2 = faker.random.words()
        cy.login('szymon.szuwalski@aidadx.io', 'peHGwSGeMLfK36JWJJfYMkJw')
        cy.get('#create-flow-btn').click()
        cy.document().then((doc) => {
            cy.stub(doc, "hidden").value(true)
        })
        cy.document().trigger('visibilitychange')
        cy.add_step(100, 100)
        cy.get('.diagram-block-container-content').first().dblclick()
        cy.get('#mat-input-0').type(title)
        cy.get('#mat-select-value-3').click()
        cy.get('#mat-option-0').click();
        cy.get('#mat-select-value-3').should('have.text', 'Komunikat')
        cy.get('#mat-input-1').type(words1)
        cy.get('p').click().type(words2);
        cy.get('#btn-save-step').click()
        cy.get('.diagram-block-container-content-title').eq(0).should('have.text', title)
        //edit second step
        cy.get('#btn-share').click()
        cy.get('#btn-build').click().blur()
        // cy.document().then((doc) => {
        //     cy.stub(doc, "hidden").value(true)
        // })
        // cy.document().trigger('visibilitychange')
        cy.add_step(100, 400)
        // cy.document().then((doc) => {
        //     cy.stub(doc, "hidden").value(true)
        // })
        // cy.document().trigger('visibilitychange')
        cy.get('#diagram_diagramLayer_svg') //.trigger('blur')
        cy.get('.diagram-block-container-content').eq(1).dblclick({
            force: true
        })
        cy.get('.mat-form-field-infix  > input').type("title")
        cy.get('.mat-select-placeholder').last().click()
        cy.get('[role="option"]').eq(1).click();
        cy.get('.mat-select-value-text').should('have.text', 'Pojedynczy wybór')
        cy.get('[formcontrolname="question"]').type("words1")
        cy.get('p').click().type("words2")
        // // cy.get('#btn-save-step') //.click()
        // // cy.get('.diagram-block-container-content').eq(1).dblclick()
        cy.get('#btn-add').click()

    });
})